package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class Testcase_001 extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "ACME_Testcases";
		testDescription = "get the vendors name";
		testNodes="Leads";
		author = "Dhanesh";
		category = "Smoke";
		dataSheetName = "TestData_TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void vendors(String UN, String PWD, String taxID)
	{
		new LoginPage()
		.enterUsername(UN)
		.enterpassord(PWD)
		.ClickLoginbutton()
		.ClickVendor()
		.SelectSearchVendor()
		.enterTaxID(taxID)
		.clickSearch()
		.getVendor();
		
	}
	
	
}
