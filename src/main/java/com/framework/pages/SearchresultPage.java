package com.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchresultPage extends ProjectMethods{
	
	public SearchresultPage()
	{
		PageFactory.initElements(driver, this);
		//have doubt on this
	}
	
	//public String ele = "//somexpth";

	@FindBy(xpath="//table[@class='table']//td[1]") WebElement table;
	
	public void getVendor()
	{	
		
		String elementText = getElementText(table);
		System.out.println(elementText);
		
	}
	
}
