package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorsearchPage extends ProjectMethods{
	
	
	public VendorsearchPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="vendorTaxID") WebElement taxID;
	@FindBy(id="buttonSearch") WebElement search;
	
	
	public VendorsearchPage enterTaxID(String data)
	{
		clearAndType(taxID, data);
		return this;
	}

	public SearchresultPage clickSearch()
	{
		click(search);
		return new SearchresultPage();
	}
}









