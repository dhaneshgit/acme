package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DasboardPage extends ProjectMethods{

	public DasboardPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//i[@class='fa fa-truck']/..") WebElement vendors;
	@FindBy(linkText="Search for Vendor") WebElement search;
	
	
	public DasboardPage ClickVendor()
	{
		Actions act = new Actions(driver);
		act.moveToElement(vendors).pause(2000).perform();
		return this;
	}
	
	public VendorsearchPage SelectSearchVendor()
	{
		click(search);
		return new VendorsearchPage();
	}
	
	
	
}
