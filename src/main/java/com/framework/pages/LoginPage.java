package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods{
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="email") WebElement UN;
	@FindBy(id="password") WebElement PWD;
	@FindBy(id="buttonLogin") WebElement Loginbutton;
	
	public LoginPage enterUsername(String data)
	{
		clearAndType(UN, data);
		return this;
	}
	
	public LoginPage enterpassord(String data)
	{
		clearAndType(PWD, data);
		return this;
	}
	
	public DasboardPage ClickLoginbutton()
	{
		click(Loginbutton);
		return new DasboardPage();
	}
	
	
	
	
	
	
	
	
}
