package reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest test;
	
	public void startReport()
	{	
		//create our html report
		html = new ExtentHtmlReporter("./report/extentReport.html");
		
		//Editing our html reports
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}
		
		public void initializestep(String TC_Name, String Descp, String Author, String Category)
		{	
		test = extent.createTest(TC_Name,Descp);
		test.assignAuthor(Author);
		test.assignCategory(Category);
		}
		
		public void logStep(String des, String status)
		{
			if(status.equalsIgnoreCase("pass"))
			{
				test.pass(des);
			}
			else if(status.equalsIgnoreCase("fail"))
			{
				test.fail(des);
				throw new RuntimeException();
			}
			else if(status.equalsIgnoreCase("warning"))
			{
				test.warning(des);
			}
		}
		
		public void endReport()
		{
		extent.flush();		
		}
}
